﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    

    public float Speed ;
    public float runspeed;
    public float walkspeed;
    public float time2Run;
    public Camera a;
    public Animator animatior;
    public Rigidbody rb;
    public CharacterController cc;

    public GameObject dancecam;
    public GameObject playercam;



    public int interpolationFramesCount = 45; // Number of frames to completely interpolate between the 2 positions
    int elapsedFrames = 0;

    
    // Start is called before the first frame update
    void Start()
    {
       
        Speed = 0;
        //dancecam.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        Moviment();
        
    }

    private void Moviment()
    {
        //Agafar quan el unity noti un axis activat: agafem els axis Horitzontal(A,D,LeftArrow,RightArrow) i els axis Verticals(W,S,UpArrow,DownArrow)
        float x = Input.GetAxisRaw("Horizontal");
        //float y = Input.GetAxisRaw("Jump");
     
        float z = Input.GetAxisRaw("Vertical");
        
        
        float interpolationRatio = (float)elapsedFrames / interpolationFramesCount;

        time2Run=interpolationRatio/2;
        //Speed= Vector3.Lerp(new Vector3(walkspeed,0,0),new Vector3(runspeed,0,0), time2Run).x;

        elapsedFrames = (elapsedFrames + 1) % (interpolationFramesCount + 1);

       

        //Moviment General: 
        if (z != 0 /*|| z != 0*/) {Speed= Vector3.Lerp(new Vector3(Speed,0,0),new Vector3(runspeed,0,0), time2Run).x;  }
        else //Speed= Vector3.Lerp(new Vector3(Speed,0,0),new Vector3(walkspeed,0,0), time2Run).x;

        if (x != 0 /*|| z != 0*/)
        {
        Speed= Vector3.Lerp(new Vector3(Speed,0,0),new Vector3(runspeed,0,0), time2Run).x;  }
        else //Speed= Vector3.Lerp(new Vector3(Speed,0,0),new Vector3(walkspeed,0,0), time2Run).x;

        if (Input.GetKey(KeyCode.LeftShift)){
            animatior.SetBool("Run", true); 
            Speed= Vector3.Lerp(new Vector3(Speed,0,0),new Vector3(runspeed,0,0), time2Run).x; 

        } else {
            animatior.SetBool("Run",false);
            Speed= Vector3.Lerp(new Vector3(Speed,0,0),new Vector3(walkspeed,0,0), time2Run).x; 
        }

        if (Input.GetKey(KeyCode.CapsLock)){
            animatior.SetBool("Crouch", true); 
            Speed=1;
        } else {
            animatior.SetBool("Crouch",false);
            //Speed=2;
        }

        
        if (Input.GetKey(KeyCode.Mouse1)) animatior.SetBool("Shoot",true);
        else animatior.SetBool("Shoot",false);




        if(animatior.GetBool("Crouch")==true&&animatior.GetBool("Shoot")==true){
            animatior.SetBool("CrouchShoot",true);
        }else{
            animatior.SetBool("CrouchShoot",false);
        }

        if(Input.GetButton("Jump")){
            animatior.SetTrigger("Jump");
            //movement.y=3f;
            //cc.Move(new Vector3(x, 5, z)*Time.deltaTime*Speed);

            /*cc.Move(rb.AddForce(transform.up,ForceMode.Impulse););*/
            rb.AddForce(transform.up,ForceMode.Force);

        }

        if (Input.GetKey(KeyCode.Q)){
            playercam.SetActive(false);
            dancecam.SetActive(true);
            animatior.SetTrigger("Dance");
            /*if (Input.GetKey(KeyCode.Q)) {
                
                dancecam.SetActive(false);
                playercam.SetActive(true);
            }*/
            
            rb.constraints=RigidbodyConstraints.FreezeAll;
            
            
        
           
            
            
        } 
        if (Input.GetKey(KeyCode.E)){
            animatior.SetTrigger("PickUp");
            rb.constraints=RigidbodyConstraints.FreezePositionX;
            rb.constraints=RigidbodyConstraints.FreezePositionZ;
        }






        //Moviment del personatge: assignem a un vector3 el axis de x i z i multipliquem la z (z+speed) per un velocitat que mes endevant anira variant.
        Vector3 movementV = new Vector3(x*Speed, 0, z * Speed*10000000);
        //Debug.Log("MovementV = " + movementV);
       // float targetAngle = Mathf.Atan2(movementV.x,movementV.z)* Mathf.Rad2Deg;
        //transform.rotation = Quaternion.Euler(0f, targetAngle, 0f);


        //Characater controller: assignem un movimant al character controller amb el vector de movimentV i un Time.deltaTime.
        //cc.Move(movementV*Time.deltaTime);
        Vector3 v3= new Vector3(x,0,z);
        float targetAngleQ = Mathf.Atan2(v3.x,v3.z)* Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, targetAngleQ, 0f);
        cc.Move(v3*Speed*Time.deltaTime);
        //BlendTree: anem al BlendTree del moviment i assignem la z del movimentV per que es el valor d'anar cap endevant.
        if (x != 0) {
            animatior.SetFloat("Blend", movementV.x);
        } else if (z!=0) {
            animatior.SetFloat("Blend", movementV.z);
        }
        
    

        

    }

}
