﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy2states : MonoBehaviour
{
    public NavMeshAgent agent;
    public Transform player;

    public Transform direction;
    public Transform path;
    public Animator anim;

    public List<Transform> pathlist;
    public int numberpath;
    // Start is called before the first frame update


    private void Start()
    {

        numberpath = -1;
    }
    private void Update()
    {

        if (numberpath == pathlist.Count -1)
        {
            numberpath = 0;
        }

        if (anim.GetBool("Attack") == false)
        {
            walk();
        }
        

        agent.SetDestination(path.position);


        //states();
    }
    public void chanengedirection()
    {
        agent.isStopped = false;
        if (path == player) {
            path = direction;
        }
        else {
            path = player;
        }



    }

    public void states() {

        if (transform.position != Vector3.zero && path == player)
        {
            anim.SetBool("Idle", false);
            anim.SetBool("Run", true);
            //transform.LookAt(path);


        }


        /* if (transform.position == player.transform.position) {
             agent.isStopped = true;
         }*/

        /* if (agent.remainingDistance<=1&& path!=player) {
             //agent.isStopped = true;
             Debug.Log("done");
             anim.SetBool("Run", false);
             anim.SetBool("Idle", true);
         }*/


    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            persecution(other);

        }
        else {
            walk();
        }
       
    }
    private void OnTriggerExit(Collider other)
    {
            
            walk();
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        
        if (collision.collider.CompareTag("Player"))
        {
            attack();
        }
    }

    public void walk()
    {
        anim.SetBool("Attack", false);
        anim.SetBool("Run", true);

        if (agent.remainingDistance <= 1)
        {
            numberpath++;
            path = pathlist[numberpath];
        }

    }

    public void attack() {

        anim.SetBool("Run", false);
        agent.isStopped = true;
        anim.SetBool("Attack", true);
        
    }

    public void persecution(Collider other) {
        anim.SetBool("Attack", false);

        anim.SetBool("Run", true);
        path = other.transform;
    }

    





}
