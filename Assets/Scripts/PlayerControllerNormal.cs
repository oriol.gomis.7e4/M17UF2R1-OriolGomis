﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerControllerNormal : MonoBehaviour
{
    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;

    private float camy = 0.0f;
    //private float camx = 0.0f;
    public List<GameObject> objectlist;
    private Vector3 moveDirection = Vector3.zero;
    
    public Enemy2states enemy;
    public UIController uic;


    private void Start()
    {
        
    }
    void Update()
    {
        camy += speed * Input.GetAxis("Mouse X");
        //camx -= jumpSpeed * Input.GetAxis("Mouse Y");
        transform.eulerAngles = new Vector3(0.0f,camy,0.0f);
        CharacterController controller = GetComponent<CharacterController>();
        if (controller.isGrounded)
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;
            if (Input.GetButton("Jump"))
                moveDirection.y = jumpSpeed;
        }
        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);

        if (Input.GetKey(KeyCode.Return)) {
           enemy.chanengedirection();
        }
    }
   
            
    
}
