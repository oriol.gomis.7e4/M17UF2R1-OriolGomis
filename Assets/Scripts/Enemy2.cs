﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy2 : MonoBehaviour
{
    public NavMeshAgent agent;
    public Transform player;
    
    public Transform direction;
    public Transform path;
    public Animator anim;
    // Start is called before the first frame update


    private void Start()
    {

        path = player;
    }
    private void Update()
    {
        
        
        agent.SetDestination(path.position);
        
        
        states();
    }
    public void chanengedirection()
    {
        agent.isStopped = false;
        if (path == player) {
            path = direction;
        }
        else { 
            path = player;
        }
        
        
           
    }

    public void states() {
        
        if (transform.position != Vector3.zero &&  path == player)
        {
            anim.SetBool("Idle", false);
            anim.SetBool("Run", true);
            //transform.LookAt(path);
            

        }
        
        
        if (transform.position == player.transform.position) {
            agent.isStopped = true;
        }

        if (agent.remainingDistance<=1&& path!=player) {
            //agent.isStopped = true;
            Debug.Log("done");
            anim.SetBool("Run", false);
            anim.SetBool("Idle", true);
        }
        

    }

    private void OnCollisionEnter(Collision collision)
    {
       if (collision.collider.CompareTag("Player"))
        {

            anim.SetBool("Run", false);
            agent.isStopped = true;
            anim.SetBool("Attack", true);
            
            
        }

        
       

        
        
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.CompareTag("Player"))
        {

            
            anim.SetBool("Attack", false);
            agent.isStopped = false;

        }
    }

    



}
