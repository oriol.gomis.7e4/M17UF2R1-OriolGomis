﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinOrLose : MonoBehaviour
{
    
    public void win(){
        SceneManager.LoadScene("win");
    }
    public void lose(){
        SceneManager.LoadScene("lose");
    }

    public void playagain() {
        SceneManager.LoadScene("Test-R1.3.1");
    }
    public void mainmenu()
    {
        SceneManager.LoadScene("MainMenu");
    }


}
