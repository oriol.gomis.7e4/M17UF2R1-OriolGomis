﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public GameObject Cake;
    public GameObject Glasses;
    public GameObject Martell;
  
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("start;");
        
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CheckObj(string name){

        switch(name){
            case "Cake":
                Cake.SetActive(true);
            break;
            case "Glasses":
                Glasses.SetActive(true);
            break;
            case "Martell":
                Martell.SetActive(true);
            break;
            
        }

    }
    
}
