﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class player2controlls : MonoBehaviour
{
    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;

    private float camy = 0.0f;
    //private float camx = 0.0f;
    public List<GameObject> objectlist;
    private Vector3 moveDirection = Vector3.zero;

    public UIController uic;
    public List scriptavlelist;

    public List<GameObject> playerequiped;

    public AudioSource au;

    public AudioClip ac;
    public AudioClip vict;
    public AudioClip nokey;

    public GameObject alert;
    public GameObject win;

    public Text txt;
    public GameObject text;
    int objccount;
    public bool key;
    void Update()
    {
        //key=true;
        playerequiped[0].transform.Rotate(new Vector3(0,1f,0),Space.Self);
        playerequiped[1].transform.Rotate(new Vector3(0,1f,0),Space.Self);
        playerequiped[2].transform.Rotate(new Vector3(0,1f,0),Space.Self);

        alert.transform.LookAt(transform.position);

        camy += speed * Input.GetAxis("Mouse X");
        //camx -= jumpSpeed * Input.GetAxis("Mouse Y");
        transform.eulerAngles = new Vector3(0.0f,camy,0.0f);
        CharacterController controller = GetComponent<CharacterController>();
        if (controller.isGrounded)
        {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= speed;
            if (Input.GetButton("Jump"))
                moveDirection.y = jumpSpeed;

        }
        moveDirection.y -= gravity * Time.deltaTime;
        controller.Move(moveDirection * Time.deltaTime);

        
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("object")) {
            Debug.Log("entrart");
            if (Input.GetKey(KeyCode.E)) {
                au.PlayOneShot(ac);
                Debug.Log("aaaa");
                GameObject go = gameObject.GetComponent<GameObject>();
                go = other.gameObject;
                
                scriptavlelist.listobject.Add(go);
                Debug.Log(go.name);
                uic.CheckObj(go.name.ToString());
                //Instantiate(go,transform.position,Quaternion.identity);
                go.SetActive(false);
                
                switch(go.name.ToString()){
                    case "Cake":
                        playerequiped[0].SetActive(true);
                        objccount++;
                    break;
                    case "Glasses":
                        playerequiped[1].SetActive(true);
                        objccount++;
                    break;
                    case "Martell":
                        playerequiped[2].SetActive(true);
                        objccount++;
                    break;
            
                }
                
            }
        }
        if(other.CompareTag("lastdoor")){
            if(key==true){
                GameObject go = gameObject.GetComponent<GameObject>();
                go = other.gameObject;
                go.SetActive(false);
                au.PlayOneShot(vict);
                win.SetActive(true);
            }else
            {
                au.PlayOneShot(nokey,0.1f);
                Debug.Log("notkey");
                text.SetActive(true);
                StartCoroutine("msgcd");
                

            }
            
        }

        if (other.CompareTag("key")) {
            GameObject go = gameObject.GetComponent<GameObject>();
            go = other.gameObject;
            go.SetActive(false);
            key = true;
        }

        if (other.CompareTag("win"))
        {
            

            SceneManager.LoadScene("Win");
        }

        if(other.CompareTag("kill"))
        {
            SceneManager.LoadScene("Lose");
        }



    }
    IEnumerator msgcd() {
        yield return new WaitForSeconds(5f);
        text.SetActive(false);
    }


    public void dead() { 
    }
}
